nums = [1,2,3,1]
var containsDuplicate = function(nums) {
    const s = new Set(nums); 
    return s.size !== nums.length
};
